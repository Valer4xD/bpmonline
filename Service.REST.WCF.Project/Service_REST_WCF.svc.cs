﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace Service.REST.WCF.Project
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service_REST_WCF
    {
        // Чтобы использовать протокол HTTP GET, добавьте атрибут [WebGet]. (По умолчанию ResponseFormat имеет значение WebMessageFormat.Json.)
        // Чтобы создать операцию, возвращающую XML,
        //     добавьте [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     и включите следующую строку в текст операции:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        public void DoWork()
        {
            // Добавьте здесь реализацию операции
            return;
        }

        // Добавьте здесь дополнительные операции и отметьте их атрибутом [OperationContract]

        [XmlSerializerFormat]
        [WebInvoke(Method = "POST", UriTemplate = "/XmlPost", BodyStyle = WebMessageBodyStyle.Bare,
        RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Json)]
        public void XmlPost(Operations operations)
        {
            string smtpServer = "smtp.yandex.ru";
            string senderAddress = "isaevvy@ya.ru";
            string senderPassword = "wnhbjdouxwlqvwyn";
            string toAddress = "valer4n1@ya.ru";

            DateTime now = DateTime.Now;
            string mounth = $"{now:MMMM}";

            string subject = $"Оплата задолженностей за период: {mounth}";
            string fromName = "";
            string body = $@"
                <!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML  4.01//EN"" ""http://www.w3.org/TR/html4/strict.dtd"">
                <html>
	                <head>
		                <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
		                <title>Operations</title>
	                </head>
	                <body>
		                <table>
			                <tr><th>Сгенерирован отчет оплаты задолженностей за период: </th>
				                <th>({mounth}, {now:yyyy})</th></tr>
			                <tr></tr>
			                <tr><th></th>
				                <th>04.10.2021</th></tr>
		                </table>
	                <body>
                </html>";

            byte[] attachment = null;
            if(null != operations)
                using(MemoryStream ms = new MemoryStream())
                using(StreamWriter writer = new StreamWriter(ms))
                using(CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.Context.RegisterClassMap<PersonOperationMap>();
                    csv.WriteRecords(operations.personOperationList);
                    writer.Flush();
                    attachment = ms.ToArray();
                }

            string fromAddress = "collection@bank.ru";
            bool isBodyHtml = true;

            (new EmailSender()).SendMail(smtpServer, senderAddress, senderPassword, new string[] { toAddress },
                subject, fromName, body, new byte[][] { attachment }, fromAddress, isBodyHtml);
        }
    }

    public class Operations { public List<PersonOperation> personOperationList; }

    public class PersonOperation : Person
    {
        public string City { get; set; }
        public string OperationType { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public decimal AmountEUR { get; set; }

        public string DateWOTime => Date.ToString("dd.MM.yyyy");
    }

    public class Person
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public int Score { get; set; }
    }

    public class PersonOperationMap : ClassMap<PersonOperation>
    {
        public PersonOperationMap()
        {
            Map(m => m.Name).Index(0).Name("ФИО");
            Map(m => m.Phone).Index(1).Name("Телефон");
            Map(m => m.City).Index(2).Name("Город");
            Map(m => m.Score).Index(3).Name("Номер счета");
            Map(m => m.Amount).Index(4).Name("Сумма");
            Map(m => m.AmountEUR).Index(5).Name("Сумма в Евро");
            Map(m => m.DateWOTime).Index(6).Name("Дата");
        }
    }

    internal class EmailSender
    {
        /// <summary>
        /// Отправка письма на почтовый ящик.
        /// </summary>
        /// <param name="smtpServer">Адрес SMTP сервера.</param>
        /// <param name="senderAddress">Адрес отправителя.</param>
        /// <param name="senderPassword">Пароль отправителя.</param>
        /// <param name="toAddresses">Адреса получателей.</param>
        /// <param name="subject">Тема письма.</param>
        /// <param name="fromName">Имя отправителя.</param>
        /// <param name="body">Сообщение.</param>
        /// <param name="attachments">Вложения.</param>
        /// <param name="fromAddress">Визуальный адрес отправителя.</param>
        /// <param name="isBodyHtml">Сообщение в формате HTML.</param>
        /// <param name="port">Порт SMTP сервера.</param>
        /// <returns>Результат отправки письма.</returns>
        internal bool SendMail(string smtpServer,
                               string senderAddress,
                               string senderPassword,
                               IEnumerable<string> toAddresses,
                               string subject = "",
                               string fromName = "",
                               string body = "",
                               IEnumerable<byte[]> attachments = null,
                               string fromAddress = "",
                               bool isBodyHtml = false,
                               ushort port = 587)
        {
            try{using(MailMessage mail = new MailMessage())
                using(SmtpClient smtpClient = new SmtpClient(smtpServer, port)){
                    smtpClient.EnableSsl = true;
                    smtpClient.Credentials = new NetworkCredential(senderAddress.Split('@')[0], senderPassword);

                    mail.Sender = new MailAddress(senderAddress);
                    foreach(string toAddress in toAddresses)
                        mail.To.Add(new MailAddress(toAddress));

                    mail.Subject = subject;
                    mail.From = new MailAddress(string.IsNullOrWhiteSpace(fromAddress) ? senderAddress : fromAddress, fromName);
                    mail.Body = body;
                    mail.IsBodyHtml = isBodyHtml;
                    
                    List<Stream> streamList = new List<Stream>();
                    if(null != attachments)
                        foreach(byte[] attachment in attachments)
                        {
                            streamList.Add(new MemoryStream(attachment));
                            mail.Attachments.Add(new Attachment(streamList[streamList.Count - 1], "Operations.csv", MediaTypeNames.Application.Octet));
                        }
                    
                    smtpClient.Send(mail);

                    foreach(Stream stream in streamList)
                        if(stream != null)
                            stream.Dispose();

                    return true;}}
            catch(Exception ex){}
            return false;
        }
    }
}

﻿using Dapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Client.REST
{
    public partial class Form1 : Form
    {
        ContainerDI containerDI = new ContainerDI();

        public Form1()
        {
            InitializeComponent();

            containerDI.RegisterType<IOperationsRepository, OperationsRepository>(
                constructorParams: new ConnectInfoDataAccess("localhost", "BankingSystem", "msroot", "msroot", 1433).
                    ConnectionString);

            XmlPost(containerDI.Resolve<IOperationsRepository>().GetPersonOperationTable());
        }

        private void XmlPost(IOperations operations)
        {
            const string codePage = "utf-8";
            XmlSerializer formatter = new XmlSerializer(typeof(Operations));
            using (MemoryStream ms = new MemoryStream())
            {
                formatter.Serialize(ms, operations);
                byte[] operationsByteArray = ms.ToArray();

                Post("http://localhost:56399/Service_REST_WCF.svc/XmlPost",
                        operationsByteArray,
                        codePage);
            }
        }

        public string Post(string destinationUrl, byte[] sentData, string codePage)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            request.Method = "POST";
            request.ContentType = $"application/xml; encoding='{codePage}'";
            request.ContentLength = sentData.Length;
            using(Stream requestStream = request.GetRequestStream())
                requestStream.Write(sentData, 0, sentData.Length);
            using(HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream, Encoding.UTF8).ReadToEnd();
                    return responseStr;
                }
            return null;
        }
    }

    public interface IOperationsRepository
    {
        Operations GetPersonOperationTable();
    }

    public class OperationsRepository : IOperationsRepository
    {
        string connectionString = null;
        public OperationsRepository(string connectionString) => this.connectionString = connectionString;

        public Operations GetPersonOperationTable()
        {
            using(IDbConnection db = new SqlConnection(connectionString))
            {
                Operations operations = new Operations();
                operations.personOperationList = db.Query<PersonOperation>(@"
                    SELECT Name, STRING_AGG(Phone, ', ') AS Phone, Score, City, OperationType, Amount, Date FROM Person AS a
                    JOIN PersonCommunication AS b ON a.Id = b.PersonId AND Phone IS NOT NULL
                    JOIN PersonOperation AS c ON a.Id = c.PersonId AND (date BETWEEN '20150601' AND '20150630')
                    WHERE City IN ('Москва', 'СПб') GROUP BY c.Id, Name, Score, City, OperationType, Amount, Date").ToList();
                return operations;
            }
        }
    }

    public interface IOperations
    {
        List<PersonOperation> personOperationList { get; set; }
    }

    [Serializable]
    public class Operations : IOperations
    {
        public List<PersonOperation> personOperationList { get; set; }
    }

    public class PersonOperation : Person
    {
        public string City;
        public string OperationType;
        public decimal Amount;
        public DateTime Date;
        public decimal AmountEUR;

        internal PersonOperation(){}

        internal PersonOperation(string name, string phone, int score,
            string city, string operationtype, decimal amount, DateTime date) :
            base(name, phone, score)
        {
            City = city;
            OperationType = operationtype;
            Amount = amount; AmountConvert("EUR", 90);
            Date = date;
        }
        internal void AmountConvert(string amountCode, int exchangeRate)
        {
            switch(amountCode)
            {
                case "EUR": AmountEUR = Math.Round(Amount / exchangeRate, 2, MidpointRounding.ToEven); break;
            }
        }
    }

    public class Person
    {
        public string Name;
        public string Phone;
        public int Score;

        internal Person(){}

        internal Person(string name, string phone, int score)
        {
            Name = name;
            Phone = phone;
            Score = score;
        }
    }

    public class ConnectInfoDataAccess
    {
        private string _url,
                       _databaseName,
                       _login,
                       _password;
        private short? _port;

        public string ConnectionString =>
            $"Server = {_url}{(_port != null ? $",{_port}" : string.Empty)}; Database = {_databaseName}; User id = {_login}; Password = {_password}";

        public ConnectInfoDataAccess(string url, string databaseName, string login, string password, short? port = null)
        {
            _url = url;
            _databaseName = databaseName;
            _login = login;
            _password = password;
            _port = port;
        }
    }

    public class ContainerDI
    {
        private IUnityContainer _main = new UnityContainer();
        public IUnityContainer Main => _main;

        public void RegisterType<TInterface, TClass>(params object[] constructorParams) =>
            Main.RegisterType(
                typeof(TInterface),
                typeof(TClass),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(constructorParams));

        public void RegisterType<TClass>(params object[] constructorParams) =>
            Main.RegisterType(
                typeof(TClass),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(constructorParams));

        public T Resolve<T>() => Main.Resolve<T>();
        public object Resolve(Type type) => Main.Resolve(type);
    }
}
